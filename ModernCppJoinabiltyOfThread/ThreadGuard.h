#ifndef _THREAD_GUARD_
#define _THREAD_GUARD_

#include <vector>
#include <thread>
using namespace std;

class ThreadGuard
{
	std::thread& t;

public: 
	explicit ThreadGuard(std::thread& mT) : t(mT) {}
	~ThreadGuard()
	{
		if (t.joinable())
		{
			t.join();
		}
	}

	ThreadGuard(ThreadGuard& const) = delete;
	ThreadGuard& operator+ (ThreadGuard& const) = delete;
};

#endif
