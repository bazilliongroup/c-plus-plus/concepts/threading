#include <thread>
#include <iostream>
#include <chrono>
#include "ThreadGuard.h"
using namespace std;

//#define JOIN_ABILITY_OF_THREADS
#define JOIN_AND_DETATCH_FUNCTIONS
#define HANDLE_JOIN_EXCEPTION_SCENARIO

void foo()
{
	this_thread::sleep_for(std::chrono::milliseconds(5000));
	cout << "Hello from foo \n";
}

void bar()
{
	this_thread::sleep_for(std::chrono::milliseconds(5000));
	cout << "Hello from bar \n";
}

void test()
{
	printf("Hello from Test\n");
}

void other_operations()
{
	cout << "This is other operation \n";
	throw runtime_error("this is a runtime error");
}

void main()
{

#ifdef JOIN_AND_DETATCH_FUNCTIONS

	thread foo_thread(foo);
#ifdef HANDLE_JOIN_EXCEPTION_SCENARIO
	ThreadGuard tg(foo_thread);
	try
	{
		other_operations();			//throws an exception
	}
	catch (...)
	{}
#else
	thread bar_thread(bar);
	bar_thread.detach();
	cout << "This is after the bar thread detach \n";

	foo_thread.join();

	cout << "This is after foo thread join \n";
#endif
#endif

#ifdef JOIN_ABILITY_OF_THREADS
	std::thread thread1(test);
//	std::thread thread1;		//A default constrcuted thread, which is non-joinable.
	
	if (thread1.joinable())
	{
		printf("Threat 1 is joinable\n");
	}
	else
	{
		printf("Threat 1 is not joinable\n");
	}

	thread1.join();

	if (thread1.joinable())
	{
		printf("Threat 1 is joinable\n");
	}
	else
	{
		printf("Threat 1 is not joinable\n");
	}
#endif
}