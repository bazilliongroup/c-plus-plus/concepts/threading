#include <iostream>
#include <thread>
#include <chrono>

using namespace std;
//#define PASSING_PARA_BETWEEN_THREADS
//#define PROBLEMS_WHEN_PASSING_REF
//#define TRANSFERRING_OWNERSHIP_BETWEEN_A_THREAD
#define SOME_USEFUL_FUNCTIONS

#ifdef PASSING_PARA_BETWEEN_THREADS
void func_1(int x, int y)
{
	cout << "X + Y = " << x + y << endl;
}
#endif

#ifdef PASSING_PARA_BETWEEN_THREADS
void func_2(int& x)
{
	while (true)
	{
		cout << "Thread  1 value of X - " << x << endl;
		this_thread::sleep_for(chrono::milliseconds(1000));
	}
}
#endif

#ifdef PROBLEMS_WHEN_PASSING_REF
void func_2(int& x)
{
	/*
	* NOTE: THE PROBLEM HERE IS THAT THE detached thread has a reference to func_1 and when func_1 ends and fuc_2 which is
	* detached tries to call the x variable from the ended thread an exception is thrown.
	*/

	while (true)
	{
		try
		{
			cout << x << endl;
			this_thread::sleep_for(chrono::milliseconds(1000));
		}
		catch (...)
		{
			throw runtime_error("this is a runtime error");
		}
	}


}

void func_1()
{
	int x = 5;
	thread thread_2(func_2, ref(x));
	thread_2.detach();
	this_thread::sleep_for(chrono::milliseconds(5000));
	cout << "thread_1 finished execution \n";
}

#endif

void foo()
{
	printf("This thread id - %d \n", std::this_thread::get_id());
}

void bar()
{
}

void get_id_test()
{
	thread thread_1(foo);
	thread thread_2(foo);
	thread thread_3(foo);
	thread thread_4;

	printf("Thread 1 id - %d \n", thread_1.get_id());
	printf("Thread 2 id - %d \n", thread_2.get_id());
	printf("Thread 3 id - %d \n", thread_3.get_id());
	printf("Thread 4 id - %d \n", thread_4.get_id());

	thread_1.join();
	thread_2.join();
	thread_3.join();

	printf("Thread 3 id - %d \n", thread_3.get_id());
}

void main()
{
#ifdef PASSING_PARA_BETWEEN_THREADS

	//passing value via copying
//	int p = 9;
//	int q = 8;
//	thread thread_1(func_1, p, q);
//	thread_1.join();

	//passing reference
	int x = 9;
	printf("Main thread value of X - %d \n", x);
	thread thread_1(func_2, std::ref(x));
	this_thread::sleep_for(chrono::milliseconds(5000));
	x = 15;
	printf("Main thread value of X has been changed to - %d \n", x);
	thread_1.join();
#endif
#ifdef PROBLEMS_WHEN_PASSING_REF

	thread thread_1(func_1);
	thread_1.join();

#endif

#ifdef TRANSFERRING_OWNERSHIP_BETWEEN_A_THREAD

	thread thread_1(foo);
	//threads are not copy assign-able nor copy constructable
	//thread thread_2 = thread_1;
	thread thread_2 = std::move(thread_1);
	thread_1 = thread(bar);		//thread_1 can now be assigned to another thread.
	thread thread_3(foo);
	//thread_1 = std::move(thread_3); //we cannot do this or else it will crash.

	thread_1.join();
	thread_2.join();
	thread_3.join();
#endif

#ifdef SOME_USEFUL_FUNCTIONS

	get_id_test();

	int allowed_threads = std::thread::hardware_concurrency();
	printf("\nAllowed thread count in my device : %d \n", allowed_threads);

#endif
	
}