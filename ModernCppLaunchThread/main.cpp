#include <iostream>
#include <thread>

/* calls threads in C++ 11 using a: 
*
* 1. fuction
* 2. function functor
* 3. lambda expresssion
*/

void foo()
{
	printf("Hello from foo function - %d\n", std::this_thread::get_id());
}

class callable_class
{
public:
	void operator()()
	{
		printf("Hello from class with function call operator - %d\n", std::this_thread::get_id());
	}
};

void main()
{
	std::thread thread1(foo);

	callable_class obj;
	std::thread thread2(obj);
	
	std::thread thread3([]
	{
		printf("Hello from lambda expression - %d\n", std::this_thread::get_id());
	});

	thread1.join();
	thread2.join();
	thread3.join();

	printf("Hello from main which is called the Main Thread - %d\n", std::this_thread::get_id());
}